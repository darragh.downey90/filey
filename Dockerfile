# Used for building and testing purposes only

FROM ubuntu:latest as build_stage

# Install tools required to build the project
# We need to run `docker build --no-cache .` to update those dependencies
RUN apt update && apt install -y software-properties-common apt-utils xz-utils build-essential wget gnupg2 gpg-agent ssh --no-install-recommends

# RUN rm -rf /var/lib/apt/lists/* && wget -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add - 

# Install bazel 
RUN apt update && apt install -y g++-8 zlib1g-dev unzip pkg-config zip python git libtool autoconf automake libkrb5-dev --no-install-recommends

RUN wget https://github.com/bazelbuild/bazel/releases/download/0.15.0/bazel-0.15.0-installer-linux-x86_64.sh && chmod +x bazel-0.15.0-installer-linux-x86_64.sh \
    && ./bazel-0.15.0-installer-linux-x86_64.sh && export PATH="$PATH:$HOME/bin"

# RUN apt autoremove gcc g++ -y

# RUN add-apt-repository "deb http://apt.llvm.org/bionic/ llvm-toolchain-bionic-6.0 main" 

# RUN apt-get update && apt-get install -y clang-6.0 lldb-6.0 lld-6.0 --no-install-recommends 

# RUN ln -s /usr/bin/clang-6.0 /usr/bin/clang && ln -s /usr/bin/clang++-6.0 /usr/bin/clang++ 

# RUN export PATH=$PATH:/usr/bin/clang && export PATH=$PATH:/usr/bin/clang++

COPY . /filey/
WORKDIR /filey

RUN /usr/bin/g++-8 --version

# RUN CC=/usr/bin/clang++ bazel build --cxxopt="-std=c++17" --cxxopt="-stdlib=libc++" --cxxopt="-lc++experimental" //:filey --verbose_failures
RUN CC=/usr/bin/g++-8 bazel build //:filey --verbose_failures --sandbox_debug
