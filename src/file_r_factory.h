/**
 *
 * MIT License
 * 
 * Copyright (c) 2018 Darragh Downey
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @brief 
 * 
 * @file file_r_factory.h
 * @author Darragh Downey
 * @date 2018-06-21
 *
 */


#ifndef FILERFACTORY_H
#define FILERFACTORY_H

#include "abstract_file_factory.h"
#include "file_r.h"

namespace filey {
    class FileRFactory: public AbstractFileFactory
    {
    public:
        FileRFactory();
        std::unique_ptr<FileRW> openFile(std::string file_location, std::string file_name, std::string extension);
        std::unique_ptr<FileRW> openFile(std::filesystem::path path_);
    };

}

#endif // FILERFACTORY_H
