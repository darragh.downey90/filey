/**
 *
 * MIT License
 * 
 * Copyright (c) 2018 Darragh Downey
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @brief 
 * 
 * @file file_r.h
 * @author Darragh Downey
 * @date 2018-06-21
 *
 */


#ifndef FILER_H
#define FILER_H

#include "file_rw.h"

namespace filey {
    class FileR: public FileRW
    {
    public:
        FileR(std::string file_location, std::string file_name, std::string extension);
        FileR(std::filesystem::path path_);

        std::string read();
        Rx::observable<std::string> readLines(); // return all lines from a file as an observable stream

        void write(std::string line);
        void writeLines(Rx::observable<std::string> lines);

        bool isOpen();
        std::string getName();
        void close();
        void seek(int pos); // Set the input position indicator of the current associated streambuf object
        int tell(); // Returns the input position indicator 
    };
}
#endif // FILER_H
