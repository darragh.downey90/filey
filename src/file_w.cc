/**
 *
 * MIT License
 * 
 * Copyright (c) 2018 Darragh Downey
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @brief 
 * 
 * @file file_w.cc
 * @author Darragh Downey
 * @date 2018-06-21
 *
 */


#include "file_w.h"

filey::FileW::FileW(std::string file_location, std::string file_name, std::string extension) : FileRW(file_location, file_name, extension)
{
    if (m_file_location_.empty())
    {
        throw std::invalid_argument("No file location given");
    }

    if (!std::filesystem::exists(this->m_file_location_))
    {
        throw std::invalid_argument("Given file location doesn't exist:\n" + this->m_file_location_);
    }

    if (m_file_name_.empty())
    {
        throw std::invalid_argument("No file name given");
    }

    if (m_extension_.empty())
    {
        throw std::invalid_argument("No file extension given");
    }

    try
    {
        this->m_file_.open(this->m_file_location_ + this->m_file_name_ + "." + this->m_extension_, std::ios::out);
    }
    catch (std::ios_base::failure &e)
    {
        std::cout << "Failed to open file for reading.\n"
                  << "Cause of issue: " << e.what() << '\n'
                  << "Error code: " << e.code() << '\n';
    }
}

std::string filey::FileW::read()
{
    std::string s;
    return s;
}

void filey::FileW::write(std::string line)
{
    if(this->isOpen()){
        this->m_file_ << line << '\n';
    } else {
        // failure!
        std::cout << line << '\n';
    }
}

void filey::FileW::writeLines(Rx::observable<std::string> lines)
{
    lines.subscribe(
        // this feels very hacky passing [this] to the lambda
        [&](std::string line){ this->write(line); },
        [](){ std::cout << "complete!" << '\n'; } 
    );
}

bool filey::FileW::isOpen()
{
    return this->m_file_.is_open();
}

std::string filey::FileW::getName()
{
    return this->m_file_name_;
}

void filey::FileW::close()
{
    this->m_file_.close();
}


void filey::FileW::seek(int pos)
{
    this->m_file_.seekp(pos);
}

int filey::FileW::tell()
{
    return this->m_file_.tellp();
}