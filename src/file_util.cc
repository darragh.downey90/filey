/**
 *
 * MIT License
 * 
 * Copyright (c) 2018 Darragh Downey
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @brief 
 * 
 * @file file_util.cc
 * @author Darragh Downey
 * @date 2018-06-21
 *
 */


#include "file_util.h"

/**
 * @brief 
 * 
 * @param files 
 */
void filey::util::FileUtil::closeFiles(const std::vector<std::unique_ptr<FileRW>> &files)
{
    try
    {
        for (std::unique_ptr<FileRW> const &f : files)
        {
            f->close();
        }
    }
    catch(std::exception &e)
    {
        std::cout << "FileUtil::closeFiles() exception:\n" << e.what() << '\n';
    }
}

/**
 * @brief For all files given open them
 * 
 * Return false if there 
 * 
 * @param files 
 * @return true 
 * @return false 
 */
bool filey::util::FileUtil::openFiles(const std::vector<std::unique_ptr<FileRW>> &files)
{
    bool open;
    for (std::unique_ptr<FileRW> const &f: files)
    {
        open = f->isOpen();
        if (!open) {
            closeFiles(files);
            return open;
        }
    }
    return open; 
}

/**
 * @brief For a given filename check if it has been created in the volume
 * It must be a file, not a directory as Docker is to handle Volume creation
 * and filesystem stuff
 * 
 * @param filename 
 * @return true 
 * @return false 
 */
bool filey::util::FileUtil::fileExists(const std::filesystem::path &path_)
{
    if (std::filesystem::is_directory(path_)) return false;

    return std::filesystem::exists(path_);
}

/**
 * @brief For a given filename, create the file if it does not already exist.
 * If intermediary directories don't exist, create them.
 * 
 * @param filename 
 * @return true 
 * @return false 
 */
bool filey::util::FileUtil::createFile(const std::string &filename)
{
    std::filesystem::path path_ = filename;

    if (fileExists(path_)) 
        return true;

    if(std::filesystem::create_directories(path_)) {
        std::unique_ptr<filey::FileWFactory> f_factory = std::make_unique<filey::FileWFactory>();
        std::unique_ptr<filey::FileRW> f = f_factory->openFile(path_);
        if (f->isOpen())
        {
            f->close();
            // f->reset();
            return true;
        }
    }
    return false;
}