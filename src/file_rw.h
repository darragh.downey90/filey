/**
 *
 * MIT License
 * 
 * Copyright (c) 2018 Darragh Downey
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @brief 
 * 
 * @file file_rw.h
 * @author Darragh Downey
 * @date 2018-06-21
 *
 */


#ifndef FILERW_H
#define FILERW_H

#include <memory>
#include <fstream>
#include <iostream>
#include <string>

// #include <boost/filesystem.hpp>
//#ifdef _MAC_OS
#include <filesystem> // this needs to replace the above

//#elif __linux
// #include <filesystem>

//#endif // end checking platforms

#include "rxcpp/rx.hpp"
namespace Rx {
using namespace rxcpp;
using namespace rxcpp::sources;
using namespace rxcpp::operators;
using namespace rxcpp::util;
}
using namespace Rx;

namespace filey {
    /**
     * @brief The FileRW class is an abstract class which parents the FileR and FileW classes.
     * It's essentially an Adapter pattern around the STL.
     * 
     */
    class FileRW
    {
    public:
        /**
         * @brief Construct a new FileRW object from individual string components
         * 
         * @param file_location 
         * @param file_name 
         * @param extension file extension
         */
        FileRW(std::string file_location, std::string file_name, std::string extension);


        /**
         * @brief Construct a new File R W object from a filesystem path
         * 
         * @param path_ 
         */
        FileRW(std::filesystem::path path_);
        
        /**
         * @brief read line from file
         * 
         * @return std::string 
         */
        virtual std::string read() = 0;

        /**
         * @brief read all lines from a given file as a "cold" observable stream, 
         * ie. wait until an observer subscribes before it begins to emit items, 
         * such an observer is guaranteed to see the whole sequence from the beginning.
         * 
         * see "Hot" and "Cold" Observables here: http://reactivex.io/documentation/observable.html
         * 
         * @return Rx::observable<std::string> 
         */
        virtual Rx::observable<std::string> readLines() = 0;

        /**
         * @brief write string to file
         * 
         * @param line 
         */
        virtual void write(std::string line) = 0;

        /**
         * @brief accept an observable stream and write to file
         * 
         * @param lines 
         */
        virtual void writeLines(Rx::observable<std::string> linesToWrite) = 0;

        /**
         * @brief check if file is open
         * 
         * @return true 
         * @return false 
         */
        virtual bool isOpen() = 0;

        /**
         * @brief Get the Name object
         * 
         * @return std::string 
         */
        virtual std::string getName() = 0;

        /**
         * @brief close the given file
         * 
         */
        virtual void close() = 0;

        /**
         * @brief Depending on the file opening type, seek will either set the input/output position indicator
         * 
         * @param pos 
         */
        virtual void seek(int pos) = 0;

        /**
         * @brief Depending on the file opening type, tell will either return the input/output position indicator
         * 
         * @return int 
         */
        virtual int tell() = 0;

        protected:
        std::string m_file_location_;
        std::string m_file_name_;
        std::string m_extension_;

        std::fstream m_file_;
    };
}
#endif // FILERW_H
