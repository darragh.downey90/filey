/**
 *
 * MIT License
 * 
 * Copyright (c) 2018 Darragh Downey
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * 
 * @brief 
 * 
 * @file file_r.cc
 * @author Darragh Downey
 * @date 2018-06-21
 *
 */


#include "file_r.h"

filey::FileR::FileR(std::string file_location, std::string file_name, std::string extension) : FileRW(file_location, file_name, extension)
{
    if (m_file_location_.empty())
        throw std::invalid_argument("");

    if (!std::filesystem::exists(this->m_file_location_))
    {
        throw std::invalid_argument("Given file location doesn't exist:\n" + this->m_file_location_);
    }

    if (m_file_name_.empty())
    {
        throw std::invalid_argument("Invalid file name");
    }
    
    if (m_extension_.empty())
    {
        throw std::invalid_argument("Invalid file extension");
    }
    
    try 
    {
        this->m_file_.open(this->m_file_location_ + this->m_file_name_ + "." + this->m_extension_, std::ios::in);
    }
    catch (std::ios_base::failure& e)
    {
        std::cout << "Failed to open file for reading.\n"
                  << "Cause of issue: " << e.what() << '\n'
                  << "Error code: " << e.code() << '\n';
    }   
}

std::string filey::FileR::read()
{
    std::string line;
    if(this->isOpen()){
        std::getline(this->m_file_, line);
    }
    return line;
}

Rx::observable<std::string> filey::FileR::readLines()
{
    auto lines = Rx::observable<>::create<std::string>(
        // see https://stackoverflow.com/questions/7895879/using-member-variable-in-lambda-capture-list-inside-a-member-function for details on why [this] was necessary
        // or see https://en.cppreference.com/w/cpp/language/lambda#Lambda_capture 
        [&](Rx::subscriber<std::string> dest)
        {
            std::string line;
            while(std::getline(m_file_, line)) 
            {
                dest.on_next(line);
            }
        }).
        publish();

    return lines;
}

void filey::FileR::write(std::string line)
{
    return;
}

bool filey::FileR::isOpen()
{
    return this->m_file_.is_open();
}

std::string filey::FileR::getName()
{
    return this->m_file_name_;
}

void filey::FileR::close()
{
    this->m_file_.close();
}

void filey::FileR::seek(int pos)
{
    this->m_file_.seekg(pos);
}

int filey::FileR::tell()
{
    return this->m_file_.tellg();
}